using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using System.Text;
using NATS.Client;
using Newtonsoft.Json;

namespace nats_dotnet_replier
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        ConnectionFactory cf;
        IConnection c;
        String subject = EnvConfigs.NATS_SUBJECT;
        String queue = EnvConfigs.NATS_Q;

        private class Data
        {
            public String name;
            public String timeStamp;
        }

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

         public override async Task StartAsync(CancellationToken cancellationToken)
        {
             cf = new ConnectionFactory();
            var options = ConnectionFactory.GetDefaultOptions();
            options.Url = EnvConfigs.NATS_URL;
            c = cf.CreateConnection(options);
            await base.StartAsync(cancellationToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping...");            
            await c.DrainAsync(1000);
            await base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Replier worker running at: {DateTimeOffset.Now}");
           
            var x = new Data();
            x.name = "Nats";
            x.timeStamp = DateTime.Now.ToString();
            string json = JsonConvert.SerializeObject(x);
            Msg replyMsg = new Msg();
            replyMsg.Data = Encoding.UTF8.GetBytes(json);            

            _logger.LogInformation($"Starting reading the requests: {DateTimeOffset.Now}");
            //asyncReceiver("foo.request", replyMsg, stoppingToken);

            while (!stoppingToken.IsCancellationRequested)
            {

                AutoResetEvent subDone = new AutoResetEvent(false);

                EventHandler<MsgHandlerEventArgs> msgHandler = (sender, args) =>
                {
                    _logger.LogInformation("Subject: " + args.Message);

                    replyMsg.Subject = args.Message.Reply;
                    c.Publish(replyMsg);
                    c.Flush();
                    subDone.Set();
                };

                using (IAsyncSubscription s = c.SubscribeAsync(subject, queue, msgHandler))
                {
                    // just wait to complete
                    _logger.LogInformation("Waiting for message...");
                    s.AutoUnsubscribe(1);
                    subDone.WaitOne();
                }

               _logger.LogInformation($"Going to sleep for {EnvConfigs.LOOP_TIMEOUT}");
                await Task.Delay(EnvConfigs.LOOP_TIMEOUT, stoppingToken);
            }
        }

        private void asyncReceiver(string subject, Msg replyMsg, CancellationToken st)
        {
            AutoResetEvent subDone = new AutoResetEvent(false);

            EventHandler<MsgHandlerEventArgs> msgHandler = (sender, args) =>
            {
                _logger.LogInformation("Inside event handler");

                replyMsg.Subject = args.Message.Reply;
                c.Publish(replyMsg);
                c.Flush();

                //if (st.CanBeCanceled)
                //subDone.Set();
            };

            using (IAsyncSubscription s = c.SubscribeAsync(subject, msgHandler))
            {
                // just wait to complete
                subDone.WaitOne();
            }
        }
    }
}
